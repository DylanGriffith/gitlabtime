package main

import (
  "fmt"
  "time"
  "net/http"
)

func main() {
  start := time.Now()
  http.Get("https://gitlab.com/")
  elapsed := time.Since(start)
  if(elapsed.Seconds() > 5 * 60) {
    fmt.Println("Duration: ", elapsed)
  }
}
