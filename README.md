# Gitlab Time

Displays response times greater than 5 minutes from https://gitlab.com/

## Installation

```
$ go get gitlab.com/DylanGriffith/gitlabtime
```

## Usage

```
$ gitlabtime
```
